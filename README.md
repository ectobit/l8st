# l8st

![Woodpecker](https://ci.codeberg.org/api/badges/ectobit/l8st/status.svg?branch=main)

cli utility to find out the latest version of some github release or the latest tag of a container image

## Example GitHub repositories

- docker/compose
- drone/drone
- drone/drone-cli
- acim/github-latest
- golang/go
- pseudomuto/protoc-gen-doc
- daixiang0/gci
- sonatype-nexus-community/nancy
- hadolint/hadolint
- hetznercloud/cli
- helm/helm
- norwoodj/helm-docs
- roboll/helmfile
- ahmetb/kubectx
- kubernetes/code-generator
- protocolbuffers/protobuf
- starship/starship
- wercker/stern
