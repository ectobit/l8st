//!
use crate::parser::Parser;
use anyhow::{bail, Result};
use http::header;
use serde::de::DeserializeOwned;
use serde::Deserialize;
use std::ffi::OsString;

const MIME_JSON: &str = "application/vnd.github.v3+json";

const PAGE_SIZE: u8 = 100;

pub struct Client<'a> {
    client: reqwest::blocking::Client,
    versions: <Vec<String> as IntoIterator>::IntoIter,
    token: Option<OsString>,
    owner: &'a str,
    repo: &'a str,
    include_tags: bool,
    page: u32,
    count: u8,
}

impl<'a> Client<'a> {
    pub fn new(owner: &'a str, repo: &'a str, include_tags: bool, token: Option<OsString>) -> Self {
        Self {
            client: reqwest::blocking::Client::new(),
            versions: vec![].into_iter(),
            token,
            owner,
            repo,
            include_tags,
            page: 0,
            count: 0,
        }
    }

    pub fn l8st(&mut self, major: Option<u64>) -> Result<String> {
        let parser = Parser::new();

        for ver in self.into_iter() {
            match ver {
                Ok(vu) => {
                    if let Some(v) = parser.parse_ver(&vu) {
                        if let Some(m) = major {
                            if v.major == m && v.pre == semver::Prerelease::EMPTY {
                                return Ok(vu);
                            }
                        }
                        if v.pre == semver::Prerelease::EMPTY {
                            return Ok(vu);
                        }
                    }
                }
                Err(err) => return Err(err),
            }
        }

        Ok("".to_string())
    }

    fn try_next(&mut self) -> Result<Option<String>> {
        if let Some(ver) = self.versions.next() {
            return Ok(Some(ver));
        }

        if self.page > 0 && self.count < PAGE_SIZE {
            return Ok(None);
        }

        self.page += 1;

        let rels = self.get::<Release>(&self.url("releases"))?;

        if rels.is_empty() {
            if self.include_tags {
                let tags = self.get::<Tag>(&self.url("tags"))?;

                if tags.is_empty() {
                    bail!("{}", "no releases or tags found")
                }

                let mut vs = vec![];

                for t in tags {
                    vs.push(t.name)
                }

                self.count = vs.len() as u8;
                self.versions = vs.into_iter();
                return Ok(self.versions.next());
            }

            bail!("{}", "no releases found")
        }

        let mut vs = vec![];

        for r in rels {
            vs.push(r.tag_name)
        }

        self.count = vs.len() as u8;
        self.versions = vs.into_iter();
        Ok(self.versions.next())
    }

    fn url(&self, query: &str) -> String {
        format!(
            "https://api.github.com/repos/{}/{}/{}?page={}&per_page={}",
            self.owner, self.repo, query, self.page, PAGE_SIZE
        )
    }

    fn get<T: DeserializeOwned>(&self, url: &str) -> Result<Vec<T>> {
        let mut req = self
            .client
            .get(url)
            .header(header::ACCEPT, MIME_JSON)
            .header(header::USER_AGENT, "l8st");

        if self.token.is_some() {
            let token = self.token.as_ref().unwrap().to_str().unwrap();
            req = req.header(header::AUTHORIZATION, format!("token {}", token));
        }

        let res = self.client.execute(req.build()?)?;

        if !res.status().is_success() {
            bail!(
                "{}",
                res.status().canonical_reason().unwrap().to_lowercase()
            )
        }

        Ok(res.json::<Response<T>>()?)
    }
}

impl<'a> Iterator for Client<'a> {
    type Item = Result<String>;

    fn next(&mut self) -> Option<Self::Item> {
        match self.try_next() {
            Ok(Some(ver)) => Some(Ok(ver)),
            Ok(None) => None,
            Err(err) => Some(Err(err)),
        }
    }
}

pub type Response<T> = Vec<T>;

#[derive(Default, Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Release {
    pub url: String,
    #[serde(rename = "assets_url")]
    pub assets_url: String,
    #[serde(rename = "upload_url")]
    pub upload_url: String,
    #[serde(rename = "html_url")]
    pub html_url: String,
    pub id: u64,
    pub author: Author,
    #[serde(rename = "node_id")]
    pub node_id: String,
    #[serde(rename = "tag_name")]
    pub tag_name: String,
    #[serde(rename = "target_commitish")]
    pub target_commitish: String,
    pub name: Option<String>,
    pub draft: bool,
    pub prerelease: bool,
    #[serde(rename = "created_at")]
    pub created_at: String,
    #[serde(rename = "published_at")]
    pub published_at: String,
    pub assets: Vec<Asset>,
    #[serde(rename = "tarball_url")]
    pub tarball_url: String,
    #[serde(rename = "zipball_url")]
    pub zipball_url: String,
    pub body: Option<String>,
    pub reactions: Option<Reactions>,
}

#[derive(Default, Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Author {
    pub login: String,
    pub id: u64,
    #[serde(rename = "node_id")]
    pub node_id: String,
    #[serde(rename = "avatar_url")]
    pub avatar_url: String,
    #[serde(rename = "gravatar_id")]
    pub gravatar_id: String,
    pub url: String,
    #[serde(rename = "html_url")]
    pub html_url: String,
    #[serde(rename = "followers_url")]
    pub followers_url: String,
    #[serde(rename = "following_url")]
    pub following_url: String,
    #[serde(rename = "gists_url")]
    pub gists_url: String,
    #[serde(rename = "starred_url")]
    pub starred_url: String,
    #[serde(rename = "subscriptions_url")]
    pub subscriptions_url: String,
    #[serde(rename = "organizations_url")]
    pub organizations_url: String,
    #[serde(rename = "repos_url")]
    pub repos_url: String,
    #[serde(rename = "events_url")]
    pub events_url: String,
    #[serde(rename = "received_events_url")]
    pub received_events_url: String,
    #[serde(rename = "type")]
    pub type_field: String,
    #[serde(rename = "site_admin")]
    pub site_admin: bool,
}

#[derive(Default, Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Asset {
    pub url: String,
    pub id: u64,
    #[serde(rename = "node_id")]
    pub node_id: String,
    pub name: String,
    pub label: Option<String>,
    pub uploader: Uploader,
    #[serde(rename = "content_type")]
    pub content_type: String,
    pub state: String,
    pub size: u64,
    #[serde(rename = "download_count")]
    pub download_count: u64,
    #[serde(rename = "created_at")]
    pub created_at: String,
    #[serde(rename = "updated_at")]
    pub updated_at: String,
    #[serde(rename = "browser_download_url")]
    pub browser_download_url: String,
}

#[derive(Default, Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Uploader {
    pub login: String,
    pub id: u64,
    #[serde(rename = "node_id")]
    pub node_id: String,
    #[serde(rename = "avatar_url")]
    pub avatar_url: String,
    #[serde(rename = "gravatar_id")]
    pub gravatar_id: String,
    pub url: String,
    #[serde(rename = "html_url")]
    pub html_url: String,
    #[serde(rename = "followers_url")]
    pub followers_url: String,
    #[serde(rename = "following_url")]
    pub following_url: String,
    #[serde(rename = "gists_url")]
    pub gists_url: String,
    #[serde(rename = "starred_url")]
    pub starred_url: String,
    #[serde(rename = "subscriptions_url")]
    pub subscriptions_url: String,
    #[serde(rename = "organizations_url")]
    pub organizations_url: String,
    #[serde(rename = "repos_url")]
    pub repos_url: String,
    #[serde(rename = "events_url")]
    pub events_url: String,
    #[serde(rename = "received_events_url")]
    pub received_events_url: String,
    #[serde(rename = "type")]
    pub type_field: String,
    #[serde(rename = "site_admin")]
    pub site_admin: bool,
}

#[derive(Default, Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Reactions {
    pub url: String,
    #[serde(rename = "total_count")]
    pub total_count: u64,
    #[serde(rename = "+1")]
    pub plus: u64,
    #[serde(rename = "-1")]
    pub minus: u64,
    pub laugh: u64,
    pub hooray: u64,
    pub confused: u64,
    pub heart: u64,
    pub rocket: u64,
    pub eyes: u64,
}

#[derive(Default, Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Tag {
    pub name: String,
    #[serde(rename = "zipball_url")]
    pub zipball_url: String,
    #[serde(rename = "tarball_url")]
    pub tarball_url: String,
    pub commit: Commit,
    #[serde(rename = "node_id")]
    pub node_id: String,
}

#[derive(Default, Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Commit {
    pub sha: String,
    pub url: String,
}
