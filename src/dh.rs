//!
use crate::parser::Parser;
use anyhow::{bail, Result};
use http::header;
use semver::Version;
use serde::Deserialize;
use std::collections::HashMap;

const PAGE_SIZE: u8 = 100;

#[derive(Debug)]
pub struct Client<'a> {
    owner: &'a str,
    repo: &'a str,
    tags: <Vec<String> as IntoIterator>::IntoIter,
    client: reqwest::blocking::Client,
    page: u32,
    total: u64,
}

impl<'a> Client<'a> {
    pub fn new(owner: &'a str, repo: &'a str) -> Self {
        Self {
            owner,
            repo,
            tags: vec![].into_iter(),
            client: reqwest::blocking::Client::new(),
            page: 0,
            total: 0,
        }
    }

    pub fn l8st(&mut self) -> Result<String> {
        let mut vs: HashMap<Version, String> = HashMap::new();

        let parser = Parser::new();

        for tag in self.into_iter() {
            match tag {
                Ok(t) => {
                    let v = parser.parse_image_tag(&t);
                    vs.insert(v, t);
                }
                Err(err) => return Err(err),
            };
        }

        let mut mv = Version::new(0, 0, 0);

        for v in vs.keys() {
            if v > &mv {
                mv = v.clone();
            }
        }

        Ok(vs.get(&mv).unwrap().to_owned())
    }

    fn try_next(&mut self) -> Result<Option<String>> {
        if let Some(tag) = self.tags.next() {
            return Ok(Some(tag));
        }

        if self.page > 0 && (self.page * PAGE_SIZE as u32) as u64 >= self.total {
            return Ok(None);
        }

        self.page += 1;

        let res = self.get(&self.url())?;

        if res.count == 0 {
            bail!("not found")
        }

        let mut vs = vec![];
        for tag in res.results {
            if &tag.tag_status != "inactive" {
                vs.push(tag.name)
            }
        }

        self.total = res.count;
        self.tags = vs.into_iter();
        Ok(self.tags.next())
    }

    fn url(&self) -> String {
        format!(
            "https://hub.docker.com/v2/repositories/{}/{}/tags/?page={}&page_size={}",
            self.owner, self.repo, self.page, PAGE_SIZE
        )
    }

    fn get(&self, url: &str) -> Result<Response> {
        let req = self
            .client
            .get(url)
            .header(header::USER_AGENT, "l8st")
            .build()?;

        let res = self.client.execute(req)?;

        if !res.status().is_success() {
            bail!(
                "{}",
                res.status().canonical_reason().unwrap().to_lowercase()
            )
        }

        Ok(res.json::<Response>()?)
    }
}

impl<'a> Iterator for Client<'a> {
    type Item = Result<String>;

    fn next(&mut self) -> Option<Self::Item> {
        match self.try_next() {
            Ok(Some(tag)) => Some(Ok(tag)),
            Ok(None) => None,
            Err(err) => Some(Err(err)),
        }
    }
}

#[derive(Default, Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Response {
    pub count: u64,
    pub next: serde_json::Value,
    pub previous: serde_json::Value,
    pub results: Vec<Tag>,
}

#[derive(Default, Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Tag {
    pub creator: u64,
    pub id: u64,
    #[serde(rename = "image_id")]
    pub image_id: serde_json::Value,
    pub images: Vec<Image>,
    #[serde(rename = "last_updated")]
    pub last_updated: Option<String>,
    #[serde(rename = "last_updater")]
    pub last_updater: u64,
    #[serde(rename = "last_updater_username")]
    pub last_updater_username: String,
    pub name: String,
    pub repository: u64,
    #[serde(rename = "full_size")]
    pub full_size: u64,
    pub v2: bool,
    #[serde(rename = "tag_status")]
    pub tag_status: String,
    #[serde(rename = "tag_last_pulled")]
    pub tag_last_pulled: Option<String>,
    #[serde(rename = "tag_last_pushed")]
    pub tag_last_pushed: Option<String>,
}

#[derive(Default, Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Image {
    pub architecture: String,
    pub features: String,
    pub variant: Option<String>,
    pub digest: Option<String>,
    pub os: String,
    #[serde(rename = "os_features")]
    pub os_features: String,
    #[serde(rename = "os_version")]
    pub os_version: serde_json::Value,
    pub size: u64,
    pub status: String,
    #[serde(rename = "last_pulled")]
    pub last_pulled: Option<String>,
    #[serde(rename = "last_pushed")]
    pub last_pushed: serde_json::Value,
}
