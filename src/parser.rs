//!
use regex::Regex;
use semver::Version;

pub struct Parser {
    tag: Regex,
    leading_zero: Regex,
}

impl Parser {
    pub fn new() -> Self {
        Self {
            tag: Regex::new(r"^\d+\.\d+.\d+").unwrap(),
            leading_zero: Regex::new(r"\.0(\d+)").unwrap(),
        }
    }

    pub fn parse_image_tag(&self, tag: &str) -> Version {
        if let Ok(t) = Version::parse(tag) {
            if t.pre == semver::Prerelease::EMPTY {
                return t;
            }
        }

        if self.tag.is_match(&tag) {
            let t = self
                .leading_zero
                .replace_all(&tag, |caps: &regex::Captures| format!(".{}", &caps[1]))
                .to_string();
            return Version::parse(&t).unwrap();
        }

        let nt = format!("{}.0", tag);
        if self.tag.is_match(&nt) {
            let t = self
                .leading_zero
                .replace_all(&nt, |caps: &regex::Captures| format!(".{}", &caps[1]))
                .to_string();
            return Version::parse(&t).unwrap();
        }

        Version::new(0, 0, 0)
    }

    pub fn parse_ver(&self, ver: &str) -> Option<Version> {
        let ver = ver.trim_start_matches('v');

        let v = self
            .leading_zero
            .replace_all(&ver, |caps: &regex::Captures| format!(".{}", &caps[1]))
            .to_string();

        match Version::parse(&v) {
            Ok(ver) => {
                if ver.pre == semver::Prerelease::EMPTY {
                    return Some(ver);
                }
            }
            Err(_) => return None,
        }

        None
    }
}
