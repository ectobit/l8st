//!
use clap::Parser;
use std::env;

mod dh;
mod gh;
mod parser;

#[derive(Parser)]
#[clap(version = "0.1.0")]
struct Opts {
    /// owner/repo, with -d flag image or owner/image
    input: String,
    /// Lookup DockerHub instead of GitHub
    #[clap(short, long)]
    docker_hub: bool,
    /// Fix major version (GitHub only)
    #[clap(short, long)]
    major: Option<u64>,
    /// Include tags together with releases (GitHub only)
    #[clap(short, long)]
    tags: bool,
}

fn main() {
    let opts: Opts = Opts::parse();

    let input = opts.input.splitn(2, "/").collect::<Vec<_>>();

    let mut owner = "";
    let mut repo = "";

    match input.len() {
        2 => {
            owner = input[0];
            repo = input[1];
        }
        1 => {
            if !opts.docker_hub {
                exit(1, "Input should be in format owner/repo");
            }
            owner = "library";
            repo = input[0];
        }
        _ => {
            exit(
                1,
                "Input should be in format owner/repo or owner/image or image",
            );
        }
    }

    if opts.docker_hub {
        let mut client = dh::Client::new(owner, repo);

        match client.l8st() {
            Ok(v) => println!("{}", v),
            Err(err) => exit(1, &err.to_string()),
        }
    } else {
        let mut client = gh::Client::new(owner, repo, opts.tags, env::var_os("GITHUB_TOKEN"));

        match client.l8st(opts.major) {
            Ok(v) => println!("{}", v),
            Err(err) => exit(1, &err.to_string()),
        }
    }
}

fn exit(code: i32, message: &str) {
    match code {
        0 => println!("{}", message),
        _ => eprintln!("{}", message),
    }
    std::process::exit(code);
}
